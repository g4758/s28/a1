

//  fetch(`https://jsonplaceholder.typicode.com/posts`)

//     .then( data => data.json() )
//     .then( data => {

//         data.forEach(element => {
//             console.log(element)
//         })
//     })    

//GET method for a single document

    // fetch(`https://jsonplaceholder.typicode.com/posts/100`, {}) //indicate the number in the URL. In this case see "100"

    // .then( response => response.json() )
    // .then( response => {

    //         console.log(response)
    //     }) 

// Post method
    // fetch(`https://jsonplaceholder.typicode.com/posts/1`,{
    //     method: "POST",
    //     headers: {
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //         title: "New Post",
    //         body: "Hellow World",
    //         userId: 1
    //     })
    // })
    // .then( response => response.json())
    // .then( response => {
    //     console.log(response)
    // })

//PUT method to update a specific document
    // fetch(`https://jsonplaceholder.typicode.com/posts`,{
    //     method: "PUT",
    //     headers: {
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //         title: "Updated Post",
    //         body: "Hellow updated post!",
    //         userId: 1
    //     })
    // })
    // .then( response => response.json())
    // .then( response => {
    //     console.log(response)
    // })

// PATCH method
    // fetch(`https://jsonplaceholder.typicode.com/posts/1`,{
    //     method: "PATCH",
    //     headers: {
    //         "Content-Type": "application/json"
    //     },
    //     body: JSON.stringify({
    //         title: "Corrected Post thru patch method",
        
    //     })
    // })
    // .then( data => data.json())
    // .then( data => {
    //     console.log(data)
    // })

// DELETE method 
    // fetch(`https://jsonplaceholder.typicode.com/posts/1`,{
    //     method: "DELETE"
    // })

// FILTERING method
    //filtering data by sending a query via the URL
    // single query: endpoint?parameter=value
    //multiple query
    fetch(`https://jsonplaceholder.typicode.com/posts`)
    .then(data => data.json())
    .then*(data => {
        console.log(data)
    })